/* renderer.ts */

import { ShapeFlags } from '@mini-jackz-vue/shared'
import { createComponentInstance, setupComponent } from './component'
import { createAppAPI } from './apiCreateApp'
import { createTextVNode, Fragment, isSameVNodeType, Text } from './vnode'
import { effect } from '@mini-jackz-vue/reactivity'
import { shouldUpdateComponent } from './componentUpdateUtils'
import { queueJobs } from './scheduler'

export function createRenderer(options) {
  // 通过解构赋值获取 createText 函数、createElement 函数、patchProp 函数和 insert 函数
  const {
    createText: hostCreateText,
    createElement: hostCreateElement,
    patchProp: hostPatchProp,
    insert: hostInsert,
    remove: hostRemove,
    setElementText: hostSetElementText
  } = options
  // 用于处理 VNode
  function render(vnode, container) {
    patch(null, vnode, container, null, null)
  }
  // 用于处理组件对应的 VNode
  function patch(n1, n2, container, parentComponent, anchor) {
    // 根据 VNode 类型的不同调用不同的函数
    // 通过 VNode 的 shapeFlag property 与枚举变量 ShapeFlags 进行与运算来判断 VNode 类型
    const { type, shapeFlag } = n2

    // 通过 VNode 的 type property 判断 VNode 类型是 Fragment 或其他
    switch (type) {
      case Fragment:
        processFragment(n1, n2, container, parentComponent, anchor)
        break
      case Text:
        processText(n1, n2, container, anchor)
        break
      default:
        // 通过 VNode 的 shapeFlag property 与枚举变量 ShapeFlags 进行与运算来判断 VNode 类型是 Element 或 Component
        if (shapeFlag & ShapeFlags.ELEMENT) {
          processElement(n1, n2, container, parentComponent, anchor)
        } else if (shapeFlag & ShapeFlags.STATEFUL_COMPONENT) {
          processComponent(n1, n2, container, parentComponent, anchor)
        }
        break
    }
  }
  // 用于处理 Text
  function processText(n1, n2, container, anchor) {
    // 通过解构赋值获取 Text 对应 VNode 的 children，即文本内容
    const { children } = n2
    // createText 函数 --- 利用 document.createTextNode() 创建文本节点
    const textNode = hostCreateText(children)
    // insert 函数 利用 Element.insertBefore() 将该节点添加到根容器/其父元素中
    hostInsert(textNode, container, anchor)
  }
  // 用于处理 Fragment
  function processFragment(n1, n2, container, parentComponent, anchor) {
    mountChildren(n2.children, container, parentComponent, anchor)
  }

  // 用于处理 Element
  function processElement(n1, n2, container, parentComponent, anchor) {
    // 若旧 VNode 不存在则初始化 Element
    if (!n1) {
      mountElement(n2, container, parentComponent, anchor)
    }
    // 否则更新 Element
    else {
      //  更新 Element
      patchElement(n1, n2, container, parentComponent, anchor)
    }
  }
  // 用于更新 Element
  function patchElement(n1, n2, container, parentComponent, anchor) {
    const oldProps = n1.props || {}
    const newProps = n2.props || {}

    // 获取旧 VNode 的 el property 并将其挂载到新 VNode 上
    const el = (n2.el = n1.el)
    patchChildren(n1, n2, el, parentComponent, anchor)
    patchProps(el, oldProps, newProps)
  }
  // 用于更新 Element 的 children
  /**
   * 新旧节点类型枚举
   *  1.  新: 文本  ==> 旧: 文本  ==> 更新文本内容
   *                    旧: 数组  ==> 卸载节点替换为文本
   *
   *  2.  新: 数组  ==> 旧: 文本  ==> 清空文本挂载新节点
   *                    旧: 数组  ==> 双端对比算法
   *
   * 只有最后一种情况存在双端对比算法,上面情况都是比较好处理的情况
   */
  function patchChildren(n1, n2, container, parentComponent, anchor) {
    // 通过结构赋值分别获得新旧 VNode 的 children 和 shapeFlag property
    const { children: c1, shapeFlag: prevShapeFlag } = n1
    const { children: c2, shapeFlag: nextShapeFlag } = n2

    // 若新 VNode 的 children 类型是 string
    if (nextShapeFlag & ShapeFlags.TEXT_CHILDREN) {
      // 同时旧 VNode 的 children 类型是 Array
      if (prevShapeFlag & ShapeFlags.ARRAY_CHILDREN) {
        // 移除旧 VNode 的 children
        unmountChildren(c1)
      }
      if (c1 !== c2) {
        hostSetElementText(container, c2)
      }
    } else if (nextShapeFlag & ShapeFlags.ARRAY_CHILDREN) {
      // 若新 VNode 的 children 类型为 Array
      // 同时旧 VNode 的 children 类型为 string
      if (prevShapeFlag & ShapeFlags.TEXT_CHILDREN) {
        // 将根容器/父元素的文本内容设置为空字符串
        hostSetElementText(container, '')
        // 将新 VNode 的 children 添加到根容器/父元素中
        mountChildren(c2, container, parentComponent, anchor)
      }
      // 同时旧 VNode 的 children 类型为 Array
      else {
        // 双端对比 快速diff 算法
        patchKeyedChildren(c1, c2, container, parentComponent, anchor)
      }
    }
  }
  // 用于针对数组情况更新 Element 的 children 针对有 key 的 diff
  function patchKeyedChildren(
    c1,
    c2,
    container,
    parentComponent,
    parentAnchor
  ) {
    const l2 = c2.length
    // 用于双端对比的指针（索引）
    let i = 0
    let e1 = c1.length - 1
    let e2 = l2 - 1

    // 从头遍历
    while (i <= e1 && i <= e2) {
      const n1 = c1[i]
      const n2 = c2[i]

      // 若两个元素是同一 VNode 则对其调用 patch 方法进行更新
      if (isSameVNodeType(n1, n2)) {
        patch(n1, n2, container, parentComponent, parentAnchor)
      }
      // 否则结束遍历，此时 i 为从头遍历时差异位置在两个数组中的索引
      else {
        break
      }

      i++
    }

    // 从尾遍历
    while (i <= e1 && i <= e2) {
      const n1 = c1[e1]
      const n2 = c2[e2]

      // 若两个元素是同一 VNode 则对其调用 patch 方法进行更新
      if (isSameVNodeType(n1, n2)) {
        patch(n1, n2, container, parentComponent, parentAnchor)
      }
      // 否则结束遍历，此时 e1 和 e2 分别为从尾遍历时差异位置在两个数组中的索引
      else {
        break
      }

      e1--
      e2--
    }
    // 若 i > e1 则说明新的数组比旧的长，将多出的元素依次向旧的中插入
    if (i > e1) {
      if (i <= e2) {
        // 要插入位置的下一个元素的索引是 e2+1
        const nextPos = e2 + 1
        // 若 e2+1 小于新的数组长度，则锚点为新的数组中索引为 e2+1 的 VNode 的 el property，否则为 null
        const anchor = nextPos < l2 ? c2[nextPos].el : null

        // 依次对子数组[i,e2]中的 VNode 调用 patch 方法进行插入
        while (i <= e2) {
          patch(null, c2[i], container, parentComponent, anchor)

          i++
        }
      }
    }
    // 若 i > e2 则说明旧的数组比新的长，将多出的元素依次从旧的中移除
    else if (i > e2) {
      // 依次移除子数组[i,e1]中的 VNode
      while (i <= e1) {
        hostRemove(c1[i].el)
        i++
      }
    } else {
      // 若 i <= e1 且 i <= e2 则说明子数组[i,e1]和子数组[i,e2]有差异
      let s = i
      // 用于保存需要调用 patch 方法的次数
      const toBePatched = e2 - s + 1
      // 用于记录调用 patch 方法的次数
      let patched = 0
      // 用于保存子数组[i,e2]中元素的索引，key 为 VNode 的 key property，value 为索引
      const keyToNewIndexMap = new Map()
      // 用于保存子数组[i,e2]中元素在旧的数组中的索引，默认为 0，在保存时加 1
      const newIndexToOldIndexMap = new Array(toBePatched)
      // 用于标志是否存在元素移动
      let moved = false
      // 用于保存遍历子数组[i,e1]时其中元素在新的数组中的最大索引
      let maxNewIndexSoFar = 0

      for (let i = 0; i < toBePatched; i++) {
        newIndexToOldIndexMap[i] = 0
      }
      // 遍历子数组[i,e2]，保存其中元素的索引
      for (let i = s; i <= e2; i++) {
        const nextChild = c2[i]

        keyToNewIndexMap.set(nextChild.key, i)
      }
      // 遍历子数组[i,e1]，依次进行处理
      for (let i = s; i <= e1; i++) {
        const prevChild = c1[i]
        // 若 patched 大于等于 toBePatched，则说明当前元素是要移除的元素，可直接移除
        if (patched >= toBePatched) {
          hostRemove(prevChild.el)

          continue
        }

        let newIndex
        // 获取在[i,e2]对应元素的索引
        if (prevChild.key != null) {
          newIndex = keyToNewIndexMap.get(prevChild.key)
        } else {
          // 遍历子数组[i,e2]获取其索引
          for (let j = s; j <= e2; j++) {
            if (isSameVNodeType(prevChild, c2[j])) {
              newIndex = j
              break
            }
          }
        }
        // 若当前元素在新的数组中的索引不存在则其不在新的数组中，可直接移除
        if (newIndex === undefined) {
          hostRemove(prevChild.el)
        } else {
          // 若当前元素在新的数组中的索引大于等于 maxNewIndexSoFar，则更新后者
          if (newIndex >= maxNewIndexSoFar) {
            maxNewIndexSoFar = newIndex
          } else {
            // 否则说明存在元素移动，将 moved 的值变为 true
            moved = true
          }
          // 保存当前元素在旧的数组中的索引
          newIndexToOldIndexMap[newIndex - s] = i + 1
          // 对当前元素调用 patch 方法进行更新
          patch(prevChild, c2[newIndex], container, parentComponent, null)

          patched++
        }
      }
      /**
       * 用于保存最长递增子序列
       * 若 moved 为 true 则存在元素移动
       * 通过 getSequence() 函数获取 newIndexToOldIndexMap 的最长递增子序列
       * 代表[i,e1]与[i,e2]新旧数组差异的下标顺序
       */
      const increasingNewIndexSequence = moved
        ? getSequence(newIndexToOldIndexMap)
        : []
      // 用于倒序遍历最长递增子序列
      let j = increasingNewIndexSequence.length - 1
      // 倒序遍历子数组[i,e2]，依次进行处理
      for (let i = toBePatched - 1; i >= 0; i--) {
        // 用于保存当前元素在新的数组中的索引
        const nextIndex = i + s
        const nextChild = c2[nextIndex]
        /**
         * 若 nextIndex+1 小于新的数组长度
         * 则锚点为新的数组中索引为 nextIndex+1 的 VNode 的 el property
         * 否则为 null
         */
        const anchor = nextIndex + 1 < l2 ? c2[nextIndex + 1].el : null
        /**
         * 若在 newIndexToOldIndexMap 中当前元素在子数组[i,e2]索引对应的值为 0
         * 则说明该元素不在旧的数组中，对其调用 patch 方法进行插入
         */
        if (newIndexToOldIndexMap[i] === 0) {
          patch(null, nextChild, container, parentComponent, anchor)
        } else if (moved) {
          /**
           * 若 j 小于 0，即最长递增子序列已遍历完
           * 或者当前元素在子数组[i,e2]索引与最长递增子序列中索引为 j 的值不相等
           * 则说明当前元素是要移动的元素
           */
          if (j < 0 || i !== increasingNewIndexSequence[j]) {
            hostInsert(nextChild.el, container, anchor)
          } else {
            j--
          }
        }
      }
    }
  }

  // 用于遍历 children，移除其中的所有 VNode
  function unmountChildren(children) {
    for (const child of children) {
      // 移除 VNode
      hostRemove(child.el)
    }
  }

  // 用于更新 Element 的 props
  function patchProps(el, oldProps, newProps) {
    if (oldProps !== newProps) {
      // 遍历新 VNode 的 props 对象
      for (const key in newProps) {
        const prev = oldProps[key]
        const next = newProps[key]

        // 若新旧 VNode 的 props 对象中的 property 或方法不相等
        if (prev !== next) {
          // 将新 VNode 的 property 或方法挂载到元素上
          hostPatchProp(el, key, next)
        }
      }

      // 遍历旧 VNode 的 props 对象
      for (const key in oldProps) {
        // 若新 VNode 的 props 对象中不包含该 property 或方法
        if (!(key in newProps)) {
          // 将元素上该 property 或方法赋值为 null
          hostPatchProp(el, key, null)
        }
      }
    }
  }
  // 用于初始化 Element
  function mountElement(vnode, container, parentComponent, anchor) {
    // 根据 Element 对应 VNode 的 type property 创建 DOM 元素并挂载到 VNode 上 (利用document.createElement创建)
    const el = (vnode.el = hostCreateElement(vnode.type))

    // 通过解构赋值获取 Element 对应 VNode 的 props 对象、shapeFlag property 和 children
    const { props, shapeFlag, children } = vnode

    // 遍历 props 对象，利用 Element.setAttribute() 将其中的 property 挂载到新元素上
    // 其中 key 作为新元素的 attribute 或 property 名，value 作为 attribute 或 property 的值
    for (const key in props) {
      const val = props[key]
      // patchProp 函数
      hostPatchProp(el, key, val)
    }

    // 通过 VNode 的 shapeFlag property 与枚举变量 ShapeFlags 进行与运算来判断 children 类型
    if (shapeFlag & ShapeFlags.TEXT_CHILDREN) {
      el.textContent = children
    } else if (shapeFlag & ShapeFlags.ARRAY_CHILDREN) {
      mountChildren(children, el, parentComponent, anchor)
    }
    // insert 函数 利用 Element.insertBefore() 将新元素添加到根容器/其父元素中
    hostInsert(el, container, anchor)
  }

  // 用于遍历 children，对其中每个 VNode 调用 patch 方法进行处理
  function mountChildren(children, container, parentComponent, anchor) {
    children.forEach(child => {
      let vnode = typeof child === 'string' ? createTextVNode(child) : child
      patch(null, vnode, container, parentComponent, anchor)
    })
  }
  // 用于处理 Component
  function processComponent(n1, n2, container, parentComponent, anchor) {
    // 若旧 VNode 不存在则初始化 Component
    if (!n1) {
      mountComponent(n2, container, parentComponent, anchor)
    }
    // 否则更新 Component
    else {
      updateComponent(n1, n2)
    }
  }
  function updateComponent(n1, n2) {
    // 获取旧 VNode 对应组件实例对象并将其挂载到新 VNode 上
    const instance = (n2.component = n1.component)

    // 若需要更新组件
    if (shouldUpdateComponent(n1, n2)) {
      // 将新 VNode 挂载到组件实例对象上
      instance.next = n2
      // 调用组件实例对象的 update 方法
      instance.update()
    }
    // 否则
    else {
      // 将旧 VNode 的 el property 挂载到新 VNode 上
      n2.el = n1.el
      // 将新 VNode 挂载到组件实例对象上
      instance.vnode = n2
    }
  }

  // 用于更新组件实例对象的 property
  function updateComponentPreRender(instance, nextVNode) {
    instance.vnode = nextVNode
    instance.next = null
    instance.props = nextVNode.props
  }
  // 用于初始化 Component
  function mountComponent(vnode, container, parentComponent, anchor) {
    // 通过组件对应的 VNode 创建组件实例对象，用于挂载 props、slots 等
    const instance = createComponentInstance(vnode, parentComponent)
    vnode.component = instance
    //处理实例组件内部
    setupComponent(instance)
    // 处理vnode树 生成副作用函数
    setupRenderEffect(instance, vnode, container, anchor)
  }

  // 用于处理 VNode 树
  function setupRenderEffect(instance, vnode, container, anchor) {
    // 利用 effect 将调用 render 函数和 patch 方法的操作收集
    instance.update = effect(
      () => {
        // 根据组件实例对象的 isMounted property 判断是初始化或更新 VNode 树
        // 若为 false 则是初始化
        if (!instance.isMounted) {
          const { proxy } = instance

          const subTree = (instance.subTree = instance.render.call(proxy))

          patch(null, subTree, container, instance, anchor)

          vnode.el = subTree.el

          // 将组件实例对象的 isMounted property 赋值为 true
          instance.isMounted = true
        }
        // 否则是更新
        else {
          //  更新 VNode 树
          // 通过解构赋值获取组件实例对象的 proxy property 和旧 VNode 树
          const { next, vnode, proxy, subTree: preSubTree } = instance

          // 调用组件实例对象中 render 函数获取新 VNode 树，同时将 this 指向指定为 proxy property，并将其挂载到组件实例对象上
          const subTree = (instance.subTree = instance.render.call(proxy))
          if (next) {
            // 将旧 VNode 的 el property 挂载到新 VNode 上
            next.el = vnode.el

            updateComponentPreRender(instance, next)
          }
          // 调用 patch 方法处理新旧 VNode 树
          patch(preSubTree, subTree, container, instance, anchor)
        }
      },
      {
        // 传入包含 scheduler 方法的对象作为第二个参数
        // 在 scheduler 方法中将组件实例对象的 update 方法保存到队列中
        scheduler() {
          queueJobs(instance.update)
        }
      }
    )
  }
  return {
    createApp: createAppAPI(render)
  }
}

// 目标：找到给定数组arr中最长的递增子序列 避免对不需要移动位置的元素进行插入，实现用于求数组中的最长递增子序列
// Get max asscending sequence of an pure number array.
// eg. [2,3,6,1,7]  -> [2,3,6,7] -> 下标 [0,1,2,4]

// Algorithum (greedy) :
// Time: O(n*logn)
// 基本思路是：采用贪心算法+二分查找，在遍历原数组的过程中，尽可能让结果子序列中的元素值增长得慢一些。
/*  具体实现：
  1.维护一个升序数组result，和一个长度与原数组arr相同的数组p；
  2.result为在遍历过程中记录递增子序列的结果数组：如果result[i] = n，表示在当前状态下，我们至少找到了一个长度为i+1的递增序列，且序列尾部的最小值为n； 
  3.每次我们从原始数组arr中获取一个元素，并使用它更新result数组时，用p[i]记录当前元素arr[i]放入result数组的时刻，放置位置pos的前一个位置result[pos-1]的值
   （因为result中元素随时会被替换，p记录了当前位置元素arr[i]插入时，递增序列前一个元素的真实值，可以在最后用于真实值的回溯查找）;
  4.可知result数组一定是一个升序数组，遍历原始数组并选取当前元素arr[i]，然后用二分查找在result数组中找到第一个大于当前元素的元素(假设为result[pos]，没有的话把当前元素push到末尾)，
     并将其替换为当前元素的值;
  5.然后我们得到结果数组的前一个元素result[pos-1]，并记录p[i]=result[pos-1];
  6.遍历所有元素后，result数组的最后一个元素一定是最长递增序列的真实值，因为它是最大值，肯定不会被替换过。而其他元素都可能在上面的过程中被替换，所以它们可能不是最终结果序列的实际值;
  7.这就是为什么当更新result中的元素时，需要在数组p中记录真实的元素值；
  8.此时从末尾开始，每次获取result数组的元素，并查找它在原始数组arr中的位置pos，最大递增子序列的前一个真实元素值一定是p[pos];
  9.重复这个步骤8，直到找到所有的真实最长递增子序列。
  
  */
function getSequence(arr: number[]): number[] {
  const p = arr.slice() //  保存原始数据
  const result = [0] //  存储最长增长子序列的索引数组
  let i, j, u, v, c
  const len = arr.length
  for (i = 0; i < len; i++) {
    const arrI = arr[i]
    if (arrI !== 0) {
      j = result[result.length - 1] //  j是子序列索引最后一项
      if (arr[j] < arrI) {
        p[i] = j //  p记录第i个位置的索引变为j
        result.push(i)
        continue
      }
      //使用二分查找法
      u = 0 //  数组的第一项
      v = result.length - 1 //  数组的最后一项
      while (u < v) {
        //  如果arrI <= arr[j] 通过二分查找，将i插入到result对应位置；u和v相等时循环停止
        c = (u + v) >> 1
        if (arr[result[c]] < arrI) {
          u = c + 1 //  移动u
        } else {
          v = c //  中间的位置大于等于i,v=c
        }
      }
      if (arrI < arr[result[u]]) {
        if (u > 0) {
          p[i] = result[u - 1] //  记录修改的索引
        }
        result[u] = i //  更新索引数组(result)
      }
    }
  }
  // 利用前驱节点重新计算result
  u = result.length
  v = result[u - 1]
  //把u值赋给result
  while (u-- > 0) {
    //  最后通过p数组对result数组进行进行修订，取得正确的索引
    result[u] = v
    v = p[v]
  }
  return result
}
