const doc = (typeof document !== 'undefined' ? document : null) as Document
export const nodeOps = {
  // 用于将元素添加到根容器/父元素中
  insert: (child, parent, anchor) => {
    parent.insertBefore(child, anchor || null)
  },
  // 用于移除元素
  remove: child => {
    // 获取当前元素的父元素
    const parent = child.parentNode
    if (parent) {
      parent.removeChild(child)
    }
  },
  // 用于创建元素
  createElement: (tag): Element => {
    // 利用 document.createElement() 创建 DOM 元素
    return doc.createElement(tag)
  },
  // 利用 document.createTextNode() 创建文本节点
  createText: text => doc.createTextNode(text),

  createComment: text => doc.createComment(text),

  setText: (node, text) => {
    node.nodeValue = text
  },
  // 用于修改元素的文本内容
  setElementText: (el, text) => {
    el.textContent = text
  },

  parentNode: node => node.parentNode as Element | null,

  nextSibling: node => node.nextSibling,

  querySelector: selector => doc.querySelector(selector),

  setScopeId(el, id) {
    el.setAttribute(id, '')
  }
}
