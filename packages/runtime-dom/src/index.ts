import { createRenderer } from '@mini-jackz-vue/runtime-core'
import { extend, isString } from '@mini-jackz-vue/shared'
import { nodeOps } from './nodeOps'
import { patchProp } from './patchProp'

const rendererOptions = /*#__PURE__*/ extend({ patchProp }, nodeOps)
/**
 * 调用 createRenderer 函数，并传入包含 createText 函数、createElement 函数、
 * patchProp 函数、insert 函数、remove 函数和 setElementText 函数的对象
 */
const renderer: any = createRenderer(rendererOptions)

function normalizeContainer(
  container: Element | ShadowRoot | string
): Element | null {
  if (isString(container)) {
    const res = document.querySelector(container)
    return res
  }
  return container as any
}
// 用于创建应用实例
export function createApp(...args) {
  // 调用 createRenderer 函数返回对象的 createApp 方法
  const app = renderer.createApp(...args)
  const { mount } = app
  app.mount = (containerOrSelector: Element | ShadowRoot | string): any => {
    const container = normalizeContainer(containerOrSelector)
    if (!container) return
    mount(container)
  }
  return app
}
export * from '@mini-jackz-vue/runtime-core'
