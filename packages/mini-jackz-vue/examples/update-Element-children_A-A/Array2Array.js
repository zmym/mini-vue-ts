/* Array2Array.js */
import { h, ref } from '../../dist/mini-jackz-vue.esm-bundler.js'
const prev = [
  h('span', { key: 'A' }, 'A'),
  h('span', { key: 'B' }, 'B'),
  h('span', { key: 'C', class: 'p2' }, 'C'),
  h('span', { key: 'D' }, 'D'),
  h('span', { key: 'E' }, 'E'),
  h('span', { key: 'G' }, 'G')
]

const next = [
  h('span', { key: 'A' }, 'A'),
  h('span', { key: 'C', class: 'p3' }, 'C'),
  h('span', { key: 'B' }, 'B'),
  h('span', { key: 'D' }, 'D'),
  h('span', { key: 'E' }, 'E'),
  h('span', { key: 'F', class: 'p3' }, 'F'),
  h('span', { key: 'G' }, 'G')
]

export default {
  name: 'Array2Array',
  setup() {
    const isUpdateA2A = ref(false)
    window.isUpdateA2A = isUpdateA2A

    return {
      isUpdateA2A
    }
  },
  render() {
    const self = this

    return h('div', {}, self.isUpdateA2A ? next : prev)
  }
}
