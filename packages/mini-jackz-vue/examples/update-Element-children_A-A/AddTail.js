/* Array2Array.js */
import { h, ref } from '../../dist/mini-jackz-vue.esm-bundler.js'
const prev = [
  h('span', { key: 'A' }, 'A'),
  h('span', { key: 'B' }, 'B'),
  h('span', { key: 'C' }, 'C')
]

const next = [
  h('span', { key: 'A' }, 'A'),
  h('span', { key: 'B' }, 'B'),
  h('span', { key: 'C' }, 'C'),
  h('span', { key: 'X', class: 'p1' }, 'X'),
  h('span', { key: 'Y', class: 'p1' }, 'Y')
]

export default {
  name: 'AddTail',
  setup() {
    const isAddTail = ref(false)
    window.isAddTail = isAddTail

    return {
      isAddTail
    }
  },
  render() {
    const self = this

    return h('div', {}, self.isAddTail ? next : prev)
  }
}
