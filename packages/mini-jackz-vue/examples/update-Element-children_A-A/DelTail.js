/* Array2Array.js */
import { h, ref } from '../../dist/mini-jackz-vue.esm-bundler.js'
const prev = [
  h('span', { key: 'A' }, 'A'),
  h('span', { key: 'B' }, 'B'),
  h('span', { key: 'C' }, 'C'),
  h('span', { key: 'D', class: 'p2' }, 'D'),
  h('span', { key: 'E', class: 'p2' }, 'E')
]

const next = [
  h('span', { key: 'A' }, 'A'),
  h('span', { key: 'B' }, 'B'),
  h('span', { key: 'C' }, 'C')
]

export default {
  name: 'DelTail',
  setup() {
    const isDelTail = ref(false)
    window.isDelTail = isDelTail

    return {
      isDelTail
    }
  },
  render() {
    const self = this

    return h('div', {}, self.isDelTail ? next : prev)
  }
}
