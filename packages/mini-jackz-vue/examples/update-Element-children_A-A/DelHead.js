/* Array2Array.js */
import { h, ref } from '../../dist/mini-jackz-vue.esm-bundler.js'
const prev = [
  h('span', { key: 'A', class: 'p2' }, 'A'),
  h('span', { key: 'B', class: 'p2' }, 'B'),
  h('span', { key: 'C' }, 'C'),
  h('span', { key: 'D' }, 'D'),
  h('span', { key: 'E' }, 'E')
]

const next = [
  h('span', { key: 'C' }, 'C'),
  h('span', { key: 'D' }, 'D'),
  h('span', { key: 'E' }, 'E')
]

export default {
  name: 'DelHead',
  setup() {
    const isDelHead = ref(false)
    window.isDelHead = isDelHead

    return {
      isDelHead
    }
  },
  render() {
    const self = this

    return h('div', {}, self.isDelHead ? next : prev)
  }
}
