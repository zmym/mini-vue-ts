/* Array2Array.js */
import { h, ref } from '../../dist/mini-jackz-vue.esm-bundler.js'
const prev = [
  h('span', { key: 'A' }, 'A'),
  h('span', { key: 'B' }, 'B'),
  h('span', { key: 'C', class: 'p2' }, 'C'),
  h('span', { key: 'D', class: 'p2' }, 'D'),
  h('span', { key: 'E' }, 'E'),
  h('span', { key: 'F' }, 'F')
]

const next = [
  h('span', { key: 'A' }, 'A'),
  h('span', { key: 'B' }, 'B'),
  h('span', { key: 'E' }, 'E'),
  h('span', { key: 'F' }, 'F')
]

export default {
  name: 'DelMid',
  setup() {
    const isDelMid = ref(false)
    window.isDelMid = isDelMid

    return {
      isDelMid
    }
  },
  render() {
    const self = this

    return h('div', {}, self.isDelMid ? next : prev)
  }
}
