/* Text2Array.js */
import { h, ref } from '../../dist/mini-jackz-vue.esm-bundler.js'
const prev = 'prevChild'
const next = [h('div', {}, 'nextChild1'), h('div', {}, 'nextChild2')]

export default {
  name: 'Text2Array',
  setup() {
    const isUpdateT2A = ref(false)
    window.isUpdateT2A = isUpdateT2A

    return {
      isUpdateT2A
    }
  },
  render() {
    const self = this

    return h('div', {}, self.isUpdateT2A ? next : prev)
  }
}
